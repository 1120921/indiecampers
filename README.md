# IndieCampers

The schematic representation of the proposed solution follow in three .png files.

CRUD - Means create read update delete methods

The ArchitectureViewLogic.png show the proposed logic view, where a rest service is used on top of the business logic, where the models are placed. This API respond the two web app's one for the administrator and other for the users. This is a syntethic separation since could be the same web app.

The SolutionDB.png contains the proposed solution for the database assuming the points 1 and 2.

The class diagram contains the classes and packages with some of the atributes and methods to implement the architecture of the solution.


Some assumptions:

1) It was assumed that a administrator may also be a user.

2) It was assumed that each local have a set of highlights that belongs only to that place. In a situation that two locals are very close, this solution is not optimal since the highligths may be shared by two different locals nearby.

# Second part

Dependency: malhal/Laravel-Geographical

* The file: database/seeds/DatabaseSeeder.php:
	
	- Contains data to populate the database. 2 Iteneraries, 4 Cities, 7 points of interest

	
To build the solution:

	a) create mysql database: indiedb

	b) php artisan migrate
	
	c) php artisan db:seed
	