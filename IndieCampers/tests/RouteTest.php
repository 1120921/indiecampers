<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RouteTest extends TestCase
{
    public function testRouteInterestPoints()
    {
        $this->get('/interest_points')
         ->seeJsonStructure([
            '*' => [
                'id', 'description', 'latitude', 'longitude', 'local_id',
                'created_at' ,'updated_at'
            ]
        ]);

    }

    public function testRouteIteneraryCities()
    {
        $this->get('/iteneraries&city')
            ->seeJsonStructure([
                '*' => [
                    'id', 'description', 'created_at' ,'updated_at'
                ]
            ]);

    }

    public function testRouteIteneraryStartEnd()
    {
        $this->get('/start=Lisboa&end=Porto')
            ->seeJsonStructure([
                '*' => [
                    'id', 'description',
                ]
            ]);

    }

    public function testRouteByCoordinates()
    {
        $this->get('/latitude=38.697&longitude=-8.421')
            ->seeJsonStructure([

                    'id', 'description', 'latitude', 'longitude', 'local_id',
                    'created_at' ,'updated_at', 'distance'

            ]);

    }

}
