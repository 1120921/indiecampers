<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //City 1
        DB::table('locals')->insert([
            'latitude' => '38.7166700l',
            'longitude' => '-9.1333300',
            'description' =>'Lisboa',
            'created_at' =>  Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        //City 2
        DB::table('locals')->insert([
            'latitude' => '41.1496100',
            'longitude' => '-8.6109900',
            'description' =>'Porto',
            'created_at' =>  Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        //City 3
        DB::table('locals')->insert([
            'latitude' => '38.8009700',
            'longitude' => '-9.3782600',
            'description' =>'Sintra',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        //City 4
        DB::table('locals')->insert([
            'latitude' => '38.6979000',
            'longitude' => '-9.4214600',
            'description' =>'Cascais',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        //Local 1
        DB::table('interest_points')->insert([
            'latitude' => '38.7166700l',
            'longitude' => '-9.2333300',
            'description' =>'Torre de Belem',
            'local_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        //Local 1
        DB::table('interest_points')->insert([
            'latitude' => '38.7166700l',
            'longitude' => '-9.2433300',
            'description' =>'Mosteiro dos Jeronimos',
            'local_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        //Local 1
        DB::table('interest_points')->insert([
            'latitude' => '38.7166700l',
            'longitude' => '-9.2633300',
            'description' =>'Chiado',
            'local_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        //Local 2
        DB::table('interest_points')->insert([
            'latitude' => '41.1496100',
            'longitude' => '-8.6209900',
            'description' =>'Torre dos Clericos',
            'local_id' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        //Local 2
        DB::table('interest_points')->insert([
            'latitude' => '41.1496100',
            'longitude' => '-8.6209900',
            'description' =>'Avenida dos Aliados',
            'local_id' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        //Local 3
        DB::table('interest_points')->insert([
            'latitude' => '38.8009700',
            'longitude' => '-9.3682600',
            'description' =>'Castelo dos Mouros',
            'local_id' => 3,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        //Local 4
        DB::table('interest_points')->insert([
            'latitude' => '38.6979000',
            'longitude' => '-9.5214600',
            'description' =>'Marina de Cascais',
            'local_id' => 4,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);

        //Itenerary 1
        DB::table('iteneraries')->insert([
            'description' => '4 cidades',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        //Itenerary 2
        DB::table('iteneraries')->insert([
            'description' => '2 cidades',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        //Pivot table
        DB::table('iteneraries_locals')->insert([
            'itenerary_id' => '1',
            'local_id' => '1'
        ]);

        //Pivot table
        DB::table('iteneraries_locals')->insert([
            'itenerary_id' => '1',
            'local_id' => '2'
        ]);
        //Pivot table
        DB::table('iteneraries_locals')->insert([
            'itenerary_id' => '1',
            'local_id' => '3'
        ]);
        //Pivot table
        DB::table('iteneraries_locals')->insert([
            'itenerary_id' => '1',
            'local_id' => '4'
        ]);

        //Pivot table
        DB::table('iteneraries_locals')->insert([
            'itenerary_id' => '2',
            'local_id' => '1'
        ]);

        //Pivot table
        DB::table('iteneraries_locals')->insert([
            'itenerary_id' => '2',
            'local_id' => '2'
        ]);



    }
}
