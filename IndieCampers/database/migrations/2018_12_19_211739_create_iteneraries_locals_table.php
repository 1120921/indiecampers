<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItenerariesLocalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iteneraries_locals', function (Blueprint $table) {
            $table->unsignedInteger('itenerary_id');
            $table->foreign('itenerary_id')->references('id')->on('iteneraries');
            $table->unsignedInteger('local_id');
            $table->foreign('local_id')->references('id')->on('locals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('iteneraries_locals');
    }
}
