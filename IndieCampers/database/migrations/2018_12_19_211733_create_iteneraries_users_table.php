<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItenerariesUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iteneraries_users', function (Blueprint $table) {
            $table->unsignedInteger('iteneraries_id');
            $table->foreign('iteneraries_id')->references('id')->on('iteneraries');
            $table->unsignedInteger('users_id');
            $table->foreign('users_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()

        {
            Schema::drop('iteneraries_users');
        }


}
