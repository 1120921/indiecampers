<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


/**
 * Class Itenerary
 * @package App\Models
 */
class Itenerary extends Model
{

    public function indexByCity() {
        $cities = DB::table('iteneraries')
            ->join('iteneraries_locals', 'iteneraries.id', '=', 'iteneraries_locals.itenerary_id')
            ->join('locals', 'locals.id', '=', 'iteneraries_locals.local_id')
            ->get();

        return $cities;
    }

    /**
     * Function with get interaries
     *
     * @param $start
     * @param $end
     * @return string
     */
    public function pointsByStartEndCities ($start, $end) {

        $id = -1;
        //Get the iteneraries list
        $iteneraries = DB::table('iteneraries')
            ->select('id')
            ->get();

        //Iterate interaries getting the start and destination city for all the iteneraties
        foreach ($iteneraries as $itenerary) {

            //Get start city
            $citiesStart = DB::table('iteneraries')
                ->where('iteneraries.id', '=', $itenerary->id)
                ->join('iteneraries_locals', 'iteneraries.id', '=', 'iteneraries_locals.itenerary_id')
                ->join('locals', 'locals.id', '=', 'iteneraries_locals.local_id')
                ->first();
            //Get destination city
            $citiesEnd = DB::table('iteneraries')
                ->where('iteneraries.id', '=', $itenerary->id)
                ->join('iteneraries_locals', 'iteneraries.id', '=', 'iteneraries_locals.itenerary_id')
                ->join('locals', 'locals.id', '=', 'iteneraries_locals.local_id')
                ->orderBy('iteneraries_locals.local_id', 'DESC')
                ->first();


            if ($citiesStart->description == $start
                && $citiesEnd->description == $end) {
                $id=$itenerary->id;
            }

        }

        //Get local points from cities for the itenerary id
        if ($id > 0) {
                $locals = DB::table('iteneraries')
                    ->select('interest_points.id', 'interest_points.description')
                    ->where('iteneraries.id', '=', $id)
                    ->join('iteneraries_locals', 'iteneraries.id', '=', 'iteneraries_locals.itenerary_id')
                    ->join('locals', 'locals.id', '=', 'iteneraries_locals.local_id')
                    ->join('interest_points', 'interest_points.local_id', '=','locals.id')
                    ->get();

        } else {
            return "No itenerary found starting in ".$start." and ending in ".$end;
        }
        return json_encode($locals);
    }

}
