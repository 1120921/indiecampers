<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Malhal\Geographical\Geographical;

/**
 * Class InterestPoint
 * @package App\Models
 */
class InterestPoint extends Model
{

    use Geographical;

    /**
     * Display interest point closer to the coordinates km
     *
     * @param $latitude
     * @param $longitude
     * @return json with the interest point and the distance in kilometers
     */
    public function pointsByCoordinates($latitude, $longitude){

        $query = Model::distance($latitude, $longitude);
        $asc = $query->orderBy('distance', 'ASC')->first();

        return $asc->toJson();
    }
}
