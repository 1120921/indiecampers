<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('welcome');
});


/**
 * Route list, no login required
 */
Route::group(['middleware' => 'guest'], function() {
    Route::get('interest_points', 'InterestPointController@index');
    Route::get('iteneraries', 'IteneraryController@index');
    Route::get('iteneraries&city', 'IteneraryController@indexByCity');
    Route::get('start={start}&end={end}', ['uses' => 'IteneraryController@pointsByStartEndCities']);
    Route::get('latitude={latitude}&longitude={longitude}', ['uses' => 'InterestPointController@pointsByCoordinates']);
});
