<?php

namespace App\Http\Controllers;

use App\Models\Itenerary;

/**
 * Class IteneraryController
 * @package App\Http\Controllers
 */
class IteneraryController extends Controller
{

    /**
     * IteneraryController constructor.
     */
    public function __construct()
    {

        $this->middleware('guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $itenerary = Itenerary::all();
        return $itenerary->toJson();

    }

    /**
     * @return mixed
     */
    public function indexByCity()
    {
        $itenerary = new Itenerary();
        return $itenerary->indexByCity();
    }

    /**
     * @param $start
     * @param $end
     * @return string
     */
    public function pointsByStartEndCities($start, $end)
    {
        $itenerary = new Itenerary();
        return $itenerary->pointsByStartEndCities($start, $end);
    }

}
