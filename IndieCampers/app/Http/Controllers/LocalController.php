<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

/**
 * Class LocalController
 * @package App\Http\Controllers
 */
class LocalController extends Controller
{
    /**
     * LocalController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');

    }

}
