<?php

namespace App\Http\Controllers;

use App\Models\InterestPoint;
use Malhal\Geographical\Geographical;

/**
 * Class InterestPointController
 * @package App\Http\Controllers
 */
class InterestPointController extends Controller
{

    /**
     * Importing library to calculate distances
     */
    use Geographical;

    /**
     * InterestPointController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');

    }

    /**
     * Display a listing of the interest points.
     * @return json with list of interest points
     */
    public function index()
    {
        return InterestPoint::all()->toJson();
    }

    /**
     * Call the method on Class InterestPoint returning
     * the closes interest point by a given coordinate (latitude and
     * longitude in decimal degrees)
     *
     * @param $latitude decimal degrees
     * @param $longitude decimal degrees
     * @return \App\Models\json
     */
    public function pointsByCoordinates($latitude, $longitude)
    {
        $interestPoint = new InterestPoint();
        return $interestPoint->pointsByCoordinates($latitude, $longitude);
    }

}
